# Changelog

## 3.0

- feat: Add Angular PWA

## 2.4

- feat: Add Angular Server Side Rendering

## 2.0

- feat: Migrate to Angular standalone

